//
//  ShotsTableViewCell.h
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//
#import "shotObject.h"
#import <UIKit/UIKit.h>

@protocol ShotsTableViewCellDelegate <NSObject>

-(void) didSelectedShot:(shotObject *) shot;

@end

@interface ShotsTableViewCell : UITableViewCell
@property (weak, nonatomic) id<ShotsTableViewCellDelegate>delegate;
@property (strong, nonatomic) shotObject *shot;
@property (strong, nonatomic) IBOutlet UIImageView *shotImageView;
@property (strong, nonatomic) IBOutlet UILabel *shotNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *shotViewsLabel;
- (IBAction)shotTapped:(id)sender;

@end
