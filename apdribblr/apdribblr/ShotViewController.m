//
//  ShotViewController.m
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import "ShotViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <DFCache/DFCache.h>

@interface ShotViewController () {
    DFCache *cache;
}

@end

@implementation ShotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    cache  = [[DFCache alloc] initWithName:@"apd"];
    NSString *shotImageName = [NSString stringWithFormat:@"%@-%@", self.shot.identifier, self.shot.title];
    UIImage *shotImage = [cache cachedObjectForKey:shotImageName ];
    if (shotImage) {
        [self.shotImageView setImage:shotImage];
    }else {
        [self.shotImageView sd_setImageWithURL:self.shot.pictureURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [cache storeObject:image forKey:shotImageName];
        }];
    }
    NSString *playerImageName = [NSString stringWithFormat:@"%@-%@", self.shot.player.identifier, self.shot.player.name];
    UIImage *playerImage = [cache cachedObjectForKey:playerImageName];
    if (playerImage) {
        [self.playerImageView setImage:playerImage];
    }else {
        if (self.shot.player.avatarURL) {
            [self.playerImageView sd_setImageWithURL:self.shot.player.avatarURL placeholderImage:[UIImage imageNamed:@"user_placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [cache storeObject:image forKey:playerImageName];
            }];
        }
        
    }
    if (!self.shot.desc || [self.shot.desc isKindOfClass:[NSNull class]]) {
        [self.shotDescription setText:@""];
    }else {
        [self.shotDescription setText:self.shot.desc];
    }
    [self.shotLikes setText:[NSString stringWithFormat:@"%@",self.shot.likes]];
    [self.shotViews setText:[NSString stringWithFormat:@"%@", self.shot.views]];
    [self.playerNameLabel setText:self.shot.player.name];
    
}

+(void)presentShotWithObject:(shotObject *)shot {
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ShotViewController *shotView = (ShotViewController *)[main instantiateViewControllerWithIdentifier:@"shotView"];
    shotView.shot = shot;
    [ShotViewController presentWithModalTransitionStyle:UIModalTransitionStyleCrossDissolve ViewController:shotView];
}
- (IBAction)dismissTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

+(void) presentWithModalTransitionStyle:(UIModalTransitionStyle) modalTransitionStyle ViewController:(id)viewController {
    
    [viewController setModalTransitionStyle:modalTransitionStyle];
    [self modalPresentation:viewController animated:YES];
}

+(void) modalPresentation:(id) viewController animated:(BOOL) animated {
    
    UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    float systemVersion = [[UIDevice currentDevice].systemVersion floatValue];
    
    if (systemVersion >= 8.0) {
        [viewController setModalPresentationStyle: UIModalPresentationOverCurrentContext];
        [rootViewController presentViewController: viewController
                                         animated: animated
                                       completion: nil];
    } else {
        
        [rootViewController presentViewController:viewController animated:NO completion:^{
            [viewController dismissViewControllerAnimated:NO completion:^{
                rootViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
                [rootViewController presentViewController:viewController animated:animated completion:nil];
                rootViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
            }];
        }];
    }
}


@end
