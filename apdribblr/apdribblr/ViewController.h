//
//  ViewController.h
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import <DFCache/DFCache.h>
#import "ShotsTableViewCell.h"
#import "ShotViewController.h"
#import "shotObject.h"
#import <SDWebImage/UIImageView+WebCache.h>

typedef enum {
    kRequestRefreshing=0,
    kRequestUpdating=1
    
} kRequest;

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ShotsTableViewCellDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) NSUInteger page;

@end

