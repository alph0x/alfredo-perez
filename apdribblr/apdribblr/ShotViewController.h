//
//  ShotViewController.h
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shotObject.h"

@interface ShotViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *shotImageView;
@property (strong, nonatomic) IBOutlet UILabel *shotLikes;
@property (strong, nonatomic) IBOutlet UILabel *shotViews;
@property (strong, nonatomic) IBOutlet UIImageView *playerImageView;
@property (strong, nonatomic) IBOutlet UILabel *playerNameLabel;
@property (strong, nonatomic) IBOutlet UITextView *shotDescription;
@property (strong, nonatomic) shotObject *shot;

+(void)presentShotWithObject:(shotObject *)shot;

+(void) presentWithModalTransitionStyle:(UIModalTransitionStyle) modalTransitionStyle ViewController:(id)viewController;

+(void) modalPresentation:(id) viewController animated:(BOOL) animated;

@end
