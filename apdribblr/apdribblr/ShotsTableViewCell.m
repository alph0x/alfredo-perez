//
//  ShotsTableViewCell.m
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import "ShotsTableViewCell.h"

@implementation ShotsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)shotTapped:(id)sender {
    [self.delegate didSelectedShot:self.shot];
}
@end
