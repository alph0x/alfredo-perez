//
//  playerObject.h
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface playerObject : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSURL *avatarURL;

+(playerObject *) initWithDictionary:(NSDictionary *) dictionary;

@end
