//
//  playerObject.m
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import "playerObject.h"

@implementation playerObject

+(playerObject *)initWithDictionary:(NSDictionary *)dictionary {
    playerObject *player = [playerObject new];
    player.identifier = [dictionary objectForKey:@"id"];
    player.name = [dictionary objectForKey:@"name"];
    player.location = [dictionary objectForKey:@"location"];
    player.avatarURL = [NSURL URLWithString:[dictionary objectForKey:@"avatar_url"]];
    
    return player;
}

@end
