//
//  ViewController.m
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import "AFNetworking.h"
#import <DFCache/DFCache.h>
#import "ViewController.h"


#define SERVER_URL NSString stringWithFormat:@"http://api.dribbble.com/shots/popular?page=%i"

@interface ViewController () {
    NSMutableArray *cachedData;
    NSDictionary *jsonData;
    NSMutableArray *parsedResults;
    DFCache *cache;
    NSUInteger page;
    AFHTTPRequestOperation *operation;
    UIRefreshControl *refreshControl;
}

@end

@implementation ViewController

@synthesize page;

- (void)viewDidLoad {
    [super viewDidLoad];
    cache  = [[DFCache alloc] initWithName:@"apd"];
    cachedData = [cache cachedObjectForKey:@"shots"];
    parsedResults = [[NSMutableArray alloc] init];
    if (cachedData) {
        parsedResults = cachedData;
        [self.tableView reloadData];
    }else {
        [self refreshTable];
    }
    refreshControl = [[UIRefreshControl alloc]init];
    
    [self.tableView addSubview:refreshControl];
    
    [refreshControl addTarget:self
                       action:@selector(refreshTable)
             forControlEvents:UIControlEventValueChanged];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset <= -40 && currentOffset>0) {
        if (parsedResults.count > 0) {
            [self updateTable];
        }
    }
}

-(void) refreshTable {
    [self performConnection:kRequestRefreshing];
}

-(void) updateTable {
    [self performConnection:kRequestUpdating];
}

-(void)performConnection:(kRequest) requestType {
    
    if (operation.executing) {
        NSLog(@"Operation in Progress");
        return;
    }
    
    switch (requestType) {
        case kRequestUpdating:{
            
        }
            break;
        case kRequestRefreshing:{
            [parsedResults removeAllObjects];
            page = 0;
        }
            break;
        default:
            break;
    }
    
    NSURL *url = [NSURL URLWithString:[SERVER_URL, (page + 1)]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        
        
        
        if (refreshControl.refreshing) {
            [refreshControl endRefreshing];
        }
        
        page = [[responseObject objectForKey:@"page"] integerValue]; //Represents actual page;
        NSArray *results = [responseObject objectForKey:@"shots"];
        if (results.count > 0) {
            [cache removeObjectForKey:@"shotsArray"];
            for (NSDictionary *d in results) {
                [parsedResults addObject:[shotObject initFromDictionary:d]];
            }
            cachedData = parsedResults;
        }
        
        
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [refreshControl endRefreshing];
        
    }];
    
    [operation start];
    
    
}

-(void)didSelectedShot:(shotObject *)shot {
    [ShotViewController presentShotWithObject:shot];
   
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    shotObject *shot = [parsedResults objectAtIndex:indexPath.row];
    ShotsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shotCell"];
    [cell setDelegate:self];
    cell.shot = shot;
    NSString *imageName = [NSString stringWithFormat:@"%@-%@",shot.identifier, shot.title];
    UIImage *image = [cache cachedObjectForKey:imageName];
    if (image) {
        [cell.shotImageView setImage:image];
    }else {
        [cell.shotImageView sd_setImageWithURL:shot.pictureURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [cache storeObject:image forKey:imageName];
            NSLog(@"Cached: %@",imageName);
        }];
    }
    
    [cell.shotNameLabel setText:shot.title];
    [cell.shotViewsLabel setText:[NSString stringWithFormat:@"%@", shot.views]];
    return cell;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return parsedResults.count;
}

@end
