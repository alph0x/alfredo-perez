//
//  shotObject.m
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import "shotObject.h"

@implementation shotObject

+(shotObject *)initFromDictionary:(NSDictionary *)dictionary {
    shotObject *shot = [shotObject new];
    shot.identifier = [dictionary objectForKey:@"id"];
    shot.title = [dictionary objectForKey:@"title"];
    shot.desc = [dictionary objectForKey:@"description"];
    shot.likes = [dictionary objectForKey:@"likes_count"];
    shot.views = [dictionary objectForKey:@"views_count"];
    shot.pictureURL = [NSURL URLWithString:[dictionary objectForKey:@"image_url"]];
    shot.player = [playerObject initWithDictionary:[dictionary objectForKey:@"player"]];
    
    return shot;
}

@end
