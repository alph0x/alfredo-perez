//
//  shotObject.h
//  apdribblr
//
//  Created by Alfredo E. Pérez L. on 10/4/15.
//  Copyright © 2015 Alfredo E. Pérez L. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "playerObject.h"

@interface shotObject : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSNumber *likes;
@property (nonatomic, strong) NSNumber *views;
@property (nonatomic, strong) NSURL *pictureURL;
@property (nonatomic, strong) playerObject *player;

+(shotObject *) initFromDictionary:(NSDictionary *) dictionary;

@end